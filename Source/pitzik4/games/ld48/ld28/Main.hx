package pitzik4.games.ld48.ld28;

import flash.events.Event;
import flash.system.Capabilities;

import flixel.FlxGame;
import flixel.FlxG;
import flixel.util.FlxTimer;

class Main extends FlxGame {
  inline public static var S_WIDTH:Int = 320;
  inline public static var S_HEIGHT:Int = 240;
  
  public var swidth(get, never):Float;
  public var sheight(get, never):Float;
  
  public function new() {
    super(S_WIDTH, S_HEIGHT, SetupState, 2);
  }
  public function stage_onResize(?event:Event):Void {
    var sw:Float = swidth;
    var sh:Float = sheight;
    #if mobile
    var scale:Float = Math.round(Math.min(sh / S_HEIGHT, sw / S_WIDTH));
    #else
    var scale:Float = Math.floor(Math.min(sh / S_HEIGHT, sw / S_WIDTH));
    #end
    if(scale == 0) {
      scale = Math.min(sh / S_HEIGHT, sw / S_WIDTH);
    }
    
    FlxG.camera.zoom = scale;
    FlxG.resizeGame(cast sw, cast sh);
    FlxG.camera.setSize(cast sw, cast sh);
  }
  
  public function get_swidth():Float {
    var sw:Float = stage.stageWidth;
    if(sw == 0) {
      sw = Capabilities.screenResolutionX;
    }
    return sw;
  }
  public function get_sheight():Float {
    var sh:Float = stage.stageHeight;
    if(sh == 0) {
      sh = Capabilities.screenResolutionY;
    }
    return sh;
  }
}
