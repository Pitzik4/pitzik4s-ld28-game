package pitzik4.games.ld48.ld28;

import flixel.FlxState;
import flixel.FlxG;
import flixel.FlxSprite;
import flixel.FlxObject;
import flixel.FlxCamera;
import flixel.group.FlxGroup;
import flixel.util.FlxPoint;
import flixel.util.FlxRect;
import flixel.tile.FlxTilemap;
import flixel.util.FlxTimer;
import openfl.Assets;
import pitzik4.games.ld48.ld28.char.Player;
import pitzik4.games.ld48.ld28.lvl.Level;
import pitzik4.games.ld48.ld28.lvl.Level1;
import pitzik4.games.ld48.ld28.lvl.Level2;
import pitzik4.games.ld48.ld28.lvl.Level3;
import pitzik4.games.ld48.ld28.lvl.Level4;
import pitzik4.games.ld48.ld28.lvl.Level5;

class PlayState extends FlxState {
  inline public static var GAME_BGC:UInt = 0xFF303030;
  inline public static var SndShot:String = "assets/snd/shot.wav";
  
  private var _point:FlxPoint;
  
  public var level:Level;
  public var player:Player;
  public var swidth:Int;
  public var sheight:Int;
  public var dialog:DialogBox;
  public var indialog:Bool = false;
  public var diapause:Bool = false;
  public var cursor:CurSprite;
  
  public var harmgroup:FlxGroup;
  
  public var dead:Int = 0;
  public var lockon(default, set):FlxObject = null;
  
  public var bullet:Bool = true;
  public var deaths:Array<String>;
  
  override public function create():Void {
    FlxG.cameras.bgColor = GAME_BGC;
    
    deaths = [];
    
    harmgroup = new FlxGroup();
    add(harmgroup);
    
    _point = new FlxPoint();
    
    swidth = Main.S_WIDTH;
    sheight = Main.S_HEIGHT;
    
    player = new Player();
    
    dialog = new DialogBox();
    
    switchLevel(new Level1(this));
    
    add(cursor = new CurSprite());
    onResize(FlxG.stage.stageWidth, FlxG.stage.stageHeight);
  }
  public function switchLevel(lvlstuff:Level) {
    remove(player);
    if(level != null) {
      level.end();
      remove(level);
    }
    level = lvlstuff;
    add(level);
    add(player);
    FlxG.worldBounds.width = level.width;
    FlxG.worldBounds.height = level.height;
    FlxG.camera.bounds = new FlxRect(0,0,level.width,level.height);
    level.begin();
    FlxG.camera.focusOn(player.getMidpoint(_point));
  }
  @:access(flixel.tile.FlxTilemap)
  override public function onResize(width:Int, height:Int):Void {
    super.onResize(width, height);
    width = cast width/FlxG.camera.zoom;
    height = cast height/FlxG.camera.zoom;
    
    var centerX:Float = FlxG.camera.scroll.x + swidth*0.5;
    var centerY:Float = FlxG.camera.scroll.y + sheight*0.5;
    swidth = width;
    sheight = height;
    FlxG.camera.scroll.x = centerX - swidth*0.5;
    FlxG.camera.scroll.y = centerY - sheight*0.5;
    
    FlxG.camera.setSize(width, height);
    
    if(lockon == null) {
      FlxG.camera.follow(player, FlxCamera.STYLE_TOPDOWN_TIGHT, _point.set(swidth*0.5,sheight*0.5), 5);
    } else {
      FlxG.camera.follow(lockon, FlxCamera.STYLE_NO_DEAD_ZONE, _point.set(swidth*0.5,sheight*0.5), 5);
    }
    
    for(buff in level.tiles._buffers) {
      buff.updateColumns(32, level.tiles.widthInTiles);
      buff.updateRows(32, level.tiles.heightInTiles);
    }
    level.tiles.setDirty();
    
    dialog.resize(width, height);
    
    FlxG.camera.focusOn(player.getMidpoint(_point));
    FlxG.camera._flashSprite.x = swidth*0.5*FlxG.camera.zoom;
    FlxG.camera._flashSprite.y = sheight*0.5*FlxG.camera.zoom;
  }
  override public function update():Void {
    if(FlxG.keyboard.pressed("ESCAPE")) {
      TitleState.title_music = false;
      FlxG.switchState(new TitleState());
    }
    if(indialog) {
      dialog.update();
      if(dialog.done) {
        indialog = diapause = false;
        dialog.clear();
      }
    }
    if(diapause) {
      cursor.update();
    } else {
      super.update();
      //harmgroup.update();
      FlxG.collide(player, level);
      //FlxG.collide(harmgroup, level);
      FlxG.overlap(player, harmgroup, harm);
      if(FlxG.keyboard.justPressed('M','X') && bullet) {
        bullet = false;
        if(!level.shoot()) {
          FlxTimer.start(1, function(timer:FlxTimer):Void {
            dialog.clear();
            dialog.str = "What a waste of a bullet.";
            dialog.icon = 1;
            dialog.displayIcon = true;
            indialog = true;
            diapause = true;
            });
        }
        FlxG.sound.play(SndShot);
        player.superanim = "shoot";
        FlxTimer.start(0.1, function(timer:FlxTimer):Void {
          player.superanim = null;
          });
      }
    }
    if(dead <= 0 && player.y >= level.height + 256) {
      FlxG.sound.play(Level2.SndFall);
      FlxG.camera.shake(0.05,0.25);
      die("Then I fell to my death. The end.");
    } else if(dead==2 && !indialog) {
      dead = 3;
      diapause = true;
      FlxG.camera.fade(0xFF000000, 1, false, function():Void {
        FlxG.camera.fade(0xFF000000, 1, true, null, true);
        FlxG.sound.playMusic(TitleState.HUNT_KNIGHT);
        dead = 0;
        diapause = false;
        switchLevel(level.fresh());
        player.mobile = true;
        lockon = null;
        });
    }
  }
  public function harm(player:FlxObject, harmer:Harmer) {
    die(harmer.getDeathMessage());
  }
  public function die(message:String):Void {
    if(dead <= 0) {
      var neverBefore:Bool = false;
      if(!Lambda.has(deaths, message)) {
        neverBefore = true;
        deaths.push(message);
      }
      dead = 1;
      FlxG.sound.music.volume = 0;
      diapause = true;
      if(neverBefore) {
        FlxTimer.start(1.5, function(timer:FlxTimer):Void {
          dead = 2;
          dialog.clear();
          dialog.str = message;
          dialog.displayIcon = false;
          indialog = true;
          diapause = true;
          });
      } else {
        dead = 2;
      }
    }
  }
  override public function draw():Void {
    super.draw();
    if(indialog) {
      dialog.cameras = cameras;
      dialog.draw();
      cursor.draw();
    }
  }
  public function set_lockon(lo:FlxObject):FlxObject {
    if(lockon != lo) {
      lockon = lo;
      if(lockon == null) {
        FlxG.camera.follow(player, FlxCamera.STYLE_TOPDOWN_TIGHT, _point.set(swidth*0.5,sheight*0.5), 5);
      } else {
        FlxG.camera.follow(lockon, FlxCamera.STYLE_TOPDOWN_TIGHT, _point.set(swidth*0.5,sheight*0.5), 5);
      }
      FlxG.camera._flashSprite.x = swidth*0.5*FlxG.camera.zoom;
      FlxG.camera._flashSprite.y = sheight*0.5*FlxG.camera.zoom;
    }
    return lockon;
  }
  public function end():Void {
    TitleState.title_music = false;
    FlxG.camera.fade(0xFF000000, 2.5, false, function():Void {
      FlxG.switchState(new TitleState());
      FlxG.camera.fade(0xFF000000, 2.5, true, null, true);
      });
  }
}
