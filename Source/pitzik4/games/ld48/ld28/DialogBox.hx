package pitzik4.games.ld48.ld28;

import flixel.FlxSprite;
import flixel.FlxG;
import flixel.util.FlxPoint;
import flixel.FlxCamera;
import flixel.text.FlxText;

import flash.display.BitmapData;
import flash.Lib;

import openfl.Assets;

class DialogBox extends FlxSprite {
  inline public static var ImgDiabox:String = "assets/gfx/diabox.png";
  inline public static var SndType:String = "assets/snd/type.wav";
  inline public static var ImgDiacons:String = "assets/gfx/diacons.png";
  
  private var _text:FlxText;
  public var str(default,set):String;
  private var ticktime:Float = 0;
  private var printed:Int = 0;
  public var done(default,null):Bool = false;
  private var diacon:FlxSprite;
  public var displayIcon(default,set):Bool;
  public var icon(get,set):Int;
  
  public function new(w:Int=Main.S_WIDTH, h:Int=Main.S_HEIGHT) {
    super();
    immovable = true;
    loadGraphic(ImgDiabox);
    scrollFactor.set();
    _text = new FlxText(0,0,288);
    _text.scrollFactor.set();
    diacon = new FlxSprite();
    diacon.loadGraphic(ImgDiacons, true, 48, 48);
    diacon.scrollFactor.set();
    resize(w, h);
    str = "";
  }
  
  @:access(flixel.text.FlxText)
  override public function update():Void {
    super.update();
    ticktime += FlxG.elapsed;
    var ticktol:Float = 0.05;
    if(FlxG.keyboard.pressed('C','ENTER')) {
      ticktol = 0.025;
    }
    while(printed < str.length && ticktime >= ticktol) {
      var c:String = str.charAt(printed);
      if(c != " ") {
        ticktime -= ticktol;
        FlxG.sound.play(SndType);
      }
      _text.text += c;
      if(_text.height > 48) {
        var s:String = _text.text;
        while(_text._textField.textHeight > 12) {
          _text.text = _text.text.substr(0,_text.text.lastIndexOf(' '));
        }
        _text.text = s.substr(_text.text.length+1);
      }
      printed++;
    }
    if(printed >= str.length && FlxG.keyboard.justPressed('C','ENTER')) {
      done = true;
    }
  }
  public function resize(w:Int, h:Int):Void {
    x = (w-width)*0.5;
    y = h-height-8;
    _text.x = x + (displayIcon ? 64 : 8);
    _text.y = y + 8;
    diacon.x = x + 8;
    diacon.y = y + 8;
  }
  override public function draw():Void {
    super.draw();
    _text.cameras = cameras;
    _text.draw();
    if(displayIcon) {
      diacon.cameras = cameras;
      diacon.draw();
    }
  }
  
  public function clear():DialogBox {
    str = "";
    _text.text = "";
    printed = 0;
    done = false;
    displayIcon = false;
    ticktime = 0;
    return this;
  }
  
  public function set_displayIcon(di:Bool):Bool {
    if(displayIcon != di) {
      displayIcon = di;
      if(di) {
        _text.x = x + 64;
        _text.width = 232;
      } else {
        _text.x = x + 8;
        _text.width = 288;
      }
    }
    return displayIcon;
  }
  public function get_icon():Int {
    return diacon.animation.frameIndex;
  }
  public function set_icon(ic:Int):Int {
    return diacon.animation.frameIndex = ic;
  }
  public function set_str(newstr:String):String {
    str = newstr + "_";
    return str;
  }
}
