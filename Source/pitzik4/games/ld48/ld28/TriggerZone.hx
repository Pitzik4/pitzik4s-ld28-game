package pitzik4.games.ld48.ld28;

import flixel.FlxObject;
import flixel.FlxG;

class TriggerZone extends FlxObject {
  public var triggered:Bool = false;
  public var inNow(get,never):Bool;
  public var watched:FlxObject;
  public var callback:Void->Void;
  
  public function new(watched:FlxObject,x:Float=0,y:Float=0,width:Float=1,height:Float=1,callback:Void->Void=null,tilesize:Float=32) {
    super(x*tilesize,y*tilesize,width*tilesize,height*tilesize);
    immovable = true;
    allowCollisions = FlxObject.NONE;
    this.watched = watched;
    this.callback = callback;
  }
  override public function update():Void {
    super.update();
    if(!triggered && inNow) {
      triggered = true;
      if(callback != null) {
        callback();
      }
    }
  }
  
  public function get_inNow():Bool {
    return overlaps(watched);
  }
}
