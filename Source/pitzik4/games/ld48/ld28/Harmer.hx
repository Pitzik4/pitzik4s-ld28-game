package pitzik4.games.ld48.ld28;

import flixel.FlxSprite;

class Harmer extends FlxSprite {
  public function new(x:Float, y:Float, ?simpleGraphic:Dynamic) {
    super(x,y,simpleGraphic);
  }
  
  public function getDeathMessage():String {
    return "I was struck down. The end.";
  }
}
