package pitzik4.games.ld48.ld28;

import flash.events.Event;
import flash.display.StageDisplayState;

import flixel.FlxState;
import flixel.FlxG;

class SetupState extends FlxState {
  public var state:FlxState;
  
  override public function create():Void {
    cast(FlxG.game, Main).stage_onResize();
    FlxG.stage.addEventListener(Event.RESIZE, cast(FlxG.game, Main).stage_onResize);
    Settings.load();
    #if desktop
    if(Settings.fullscreen) {
      FlxG.stage.displayState = StageDisplayState.FULL_SCREEN_INTERACTIVE;
    }
    #end
    FlxG.log.redirectTraces = Settings.redirectTraces;
  }
  override public function update():Void {
    if(state == null) {
      state = new TitleState();
    }
    FlxG.switchState(state);
  }
}
