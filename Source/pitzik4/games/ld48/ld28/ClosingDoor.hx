package pitzik4.games.ld48.ld28;

import flixel.FlxSprite;
import flixel.FlxG;
import pitzik4.games.ld48.ld28.boss.Monorail;

class ClosingDoor extends FlxSprite {
  public var bottom:Float;
  public var h:Float;
  
  public function new(x:Float, y:Float, height:Int, bottom:Float, seconds:Float) {
    super(x,y);
    this.h = height;
    this.bottom = bottom;
    makeGraphic(32, height, 0xFF000000);
    velocity.y = (bottom-y)/seconds;
    immovable = true;
  }
  override public function update():Void {
    super.update();
    if(y >= bottom && velocity.y > 0) {
      y = bottom;
      velocity.y = 0;
      FlxG.sound.play(Monorail.SndStop);
      FlxG.camera.shake(0.0025, 0.1);
    }
  }
}
