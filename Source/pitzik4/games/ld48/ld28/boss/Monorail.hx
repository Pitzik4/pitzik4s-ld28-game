package pitzik4.games.ld48.ld28.boss;

import flixel.FlxSprite;
import flixel.group.FlxGroup;
import flixel.FlxG;
import flixel.FlxObject;
import flixel.system.FlxSound;
import pitzik4.games.ld48.ld28.lvl.Level1;
import pitzik4.games.ld48.ld28.PlayState;

import openfl.Assets;

class Monorail extends FlxGroup {
  inline public static var ImgRail:String = "assets/gfx/monorail.png";
  inline public static var SndRail:String = "assets/snd/monorail.wav";
  inline public static var SndStop:String = "assets/snd/monostop.wav";
  
  public var rail:FlxSprite;
  public var ob1:FlxObject;
  public var ob2:FlxObject;
  public var mrbad:FlxSprite;
  public var owner:PlayState;
  public var whirr:FlxSound;
  public var breaktime:Float = 0;
  
  public var dropping:Bool = true;
  
  public var dir:UInt = FlxObject.LEFT;
  
  private var casings:FlxGroup;
  
  public var left:Float;
  public var right:Float;
  
  public var going:Bool = false;
  
  public var velocity:Float = 0;
  
  public var casetime:Float = 0;
  
  public var stopping:Bool = false;
  public var stopcallback:Void->Void = null;
  
  public var x(default,set):Float;
  public var y(default,set):Float;
  
  public function new(x:Float = 0, y:Float = 0, ?owner:PlayState, left:Float = 0) {
    super();
    
    this.owner = owner;
    
    this.left = left;
    this.right = x;
    
    casings = new FlxGroup(8);
    
    whirr = FlxG.sound.load(SndRail, 1, true);
    
    mrbad = new FlxSprite(16,-16);
    mrbad.loadGraphic(Level1.ImgMrBad, true, true);
    mrbad.animation.add("idle", [0]);
    mrbad.animation.add("teleport", [8,8,8,8,9,10,11,12,13,14,15,18], 10, false);
    mrbad.animation.play("idle");
    mrbad.facing = FlxObject.LEFT;
    mrbad.immovable = true;
    add(mrbad);
    rail = new FlxSprite(0,0,ImgRail);
    rail.height = 16;
    rail.offset.y = 16;
    rail.y = 16;
    rail.immovable = true;
    add(rail);
    ob1 = new FlxObject(0,0,4,32);
    ob1.immovable = true;
    add(ob1);
    ob2 = new FlxObject(60,0,4,32);
    ob2.immovable = true;
    add(ob2);
    moveTo(x,y);
  }
  
  override public function update():Void {
    super.update();
    x += velocity * FlxG.elapsed;
    if(going) {
      if(breaktime > 0) {
        velocity = 0;
        breaktime -= FlxG.elapsed;
      } else {
        var done:Bool = false;
        if(dir == FlxObject.LEFT) {
          velocity = -256;
          done = x <= left;
          if(done) {
            dir = FlxObject.RIGHT;
            x = left;
          }
        } else {
          velocity = 256;
          done = x >= right;
          if(done) {
            dir = FlxObject.LEFT;
            x = right;
            if(stopping) {
              stopping = going = false;
              velocity = 0;
              if(stopcallback != null) {
                stopcallback();
              }
            }
          }
        }
        if(done) {
          FlxG.sound.play(SndStop);
          breaktime = 0.25;
        }
      }
      casetime += FlxG.elapsed;
      if(casetime >= 0.3) {
        casetime -= 0.3;
        if(dropping) {
          var casing:Casing = cast casings.recycle(Casing, [owner.harmgroup]);
          casing.exists = true;
          casing.x = x + 24;
          casing.y = y + 4;
          casing.velocity.set();
          owner.harmgroup.add(casing);
        }
      }
    }
  }
  
  public function start():Void {
    going = true;
  }
  public function stop(?callback:Void->Void):Void {
    stopping = true;
    stopcallback = callback;
  }
  
  public function moveTo(x:Float, y:Float):Void {
    this.x = x;
    this.y = y;
  }
  public function set_x(newx:Float):Float {
    rail.x = ob1.x = newx;
    ob2.x = newx + 60;
    mrbad.x = newx + 16;
    x = newx;
    return x;
  }
  public function set_y(newy:Float):Float {
    rail.y = newy + 16;
    ob1.y = ob2.y = newy;
    mrbad.y = newy - 16;
    y = newy;
    return y;
  }
  
  public function eject_bad():Void {
    remove(mrbad);
    mrbad = null;
  }
}
