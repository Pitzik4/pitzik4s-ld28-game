package pitzik4.games.ld48.ld28.boss;

import pitzik4.games.ld48.ld28.Harmer;
import flixel.group.FlxGroup;
import flixel.FlxObject;

class Casing extends Harmer {
  inline public static var ImgCasing:String = "assets/gfx/casing.png";
  
  public var harmgroup:FlxGroup;
  
  public function new(harmgroup:FlxGroup) {
    super(0, 0, ImgCasing);
    this.harmgroup = harmgroup;
    acceleration.y = 300;
    width = height = 8;
  }
  
  override public function update():Void {
    super.update();
  }
  
  override public function getDeathMessage():String {
    return "I was killed by a blow to the head with the casing of a miniature nuclear reactor. What a pointless existence.";
  }
}
