package pitzik4.games.ld48.ld28.char;

import flixel.FlxSprite;
import pitzik4.games.ld48.ld28.lvl.Level1;
import flixel.FlxObject;
import flixel.util.FlxTimer;

class DeadBad extends FlxSprite {
  public var ypos:Float;
  
  public function new(x:Float, y:Float, dir:UInt = FlxObject.LEFT) {
    super(x,y);
    ypos = y;
    loadGraphic(Level1.ImgMrBad, true, true, 32, 32);
    facing = dir;
    allowCollisions = FlxObject.NONE;
    animation.add("death", [16]);
    animation.add("dead", [17]);
    animation.play("death");
    acceleration.y = 300;
    velocity.y = -60;
    if(dir == FlxObject.LEFT) {
      velocity.x = 60;
    } else {
      velocity.x = -60;
    }
  }
  override public function update():Void {
    super.update();
    if(y > ypos) {
      acceleration.set();
      velocity.set();
      y = ypos;
      animation.play("dead");
    }
  }
}
