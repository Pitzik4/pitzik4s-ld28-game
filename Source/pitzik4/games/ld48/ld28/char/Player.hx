package pitzik4.games.ld48.ld28.char;

import flixel.FlxSprite;
import flixel.FlxG;
import flixel.FlxObject;

import openfl.Assets;

class Player extends FlxSprite {
  inline public static var ImgChar:String = "assets/gfx/char.png";
  inline public static var SndJump:String = "assets/snd/jump.wav";
  inline public static var SndStep:String = "assets/snd/step.wav";
  inline public static var RIGHT:UInt = FlxObject.RIGHT;
  inline public static var LEFT:UInt = FlxObject.LEFT;
  inline public static var MOVE_SPEED:Float = 60;
  inline public static var JUMP_TIME:Float = 0.75;
  //inline public static var JUMP_TIME:Float = 5;
  
  private var _superanim:String = null;
  public var superanim:String = null;
  public var idleanim:String = null;
  public var jumpytime:Float = JUMP_TIME;
  public var mobile:Bool = true;
  
  public function new(x:Float = 0, y:Float = 0) {
    super();
    
    this.x = x; this.y = y;
    
    loadGraphic(ImgChar, true, true, 32, 32);
    animation.add("idle", [0]);
    animation.add("jump", [3]);
    animation.add("fall", [4]);
    animation.add("walk", [1,0,2,0], 6);
    animation.add("liedown", [5]);
    animation.add("wakeup", [5, 6, 6, 7, 7, 6], 1, false);
    animation.add("shoot", [8]);
    animation.add("teleport", [9,9,9,9,10,11,12,13,14,15,16], 10, false);
    animation.play("idle");
    this.facing = RIGHT;
    
    maxVelocity.x = MOVE_SPEED;
    maxVelocity.y = 240;
    drag.x = MOVE_SPEED*8;
    acceleration.y = 360;
    
    width = 12;
    offset.set(10,0);
  }
  
  override public function update():Void {
    if(mobile) {
      if(FlxG.keyboard.pressed('D','RIGHT')) {
        acceleration.x = MOVE_SPEED*8;
        facing = RIGHT;
      } else if(FlxG.keyboard.pressed('A','LEFT')) {
        acceleration.x = -MOVE_SPEED*8;
        facing = LEFT;
      } else {
        acceleration.x = 0;
      }
      if(isTouching(FlxObject.DOWN)) {
        jumpytime = JUMP_TIME;
      }
      if(FlxG.keyboard.pressed('W','UP','Z','SPACE') && jumpytime >= 0) {
        if(jumpytime == JUMP_TIME) {
          FlxG.sound.play(SndJump);
        }
        velocity.y = -240 * jumpytime;
        jumpytime -= FlxG.elapsed;
      } else if(!isTouching(FlxObject.DOWN)) {
        jumpytime = -1;
      }
    } else {
      acceleration.x = 0;
    }
    if(superanim == null) {
      if(velocity.y == 0) {
        if(acceleration.x == 0) {
          if(idleanim == null) {
            animation.play("idle");
          } else {
            animation.play(idleanim);
          }
        } else {
          animation.play("walk");
        }
      } else {
        if(velocity.y > 0) {
          animation.play("fall");
        } else {
          animation.play("jump");
        }
      }
    } else if(superanim != _superanim) {
      animation.play(superanim);
    }
    _superanim = superanim;
    var fr:Int = animation.frameIndex;
    super.update();
    if(animation.frameIndex == 0 && fr != 0) {
      FlxG.sound.play(SndStep);
    }
  }
}
