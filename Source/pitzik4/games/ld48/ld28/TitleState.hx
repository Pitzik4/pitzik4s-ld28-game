package pitzik4.games.ld48.ld28;

#if desktop
import flash.system.System;
#end

import flixel.FlxState;
import flixel.FlxG;
import flixel.group.FlxSpriteGroup;
import flixel.text.FlxText;
import flixel.ui.FlxButton;
import flixel.FlxSprite;

class TitleState extends FlxState {
  inline public static var MENU_BGC:UInt = 0xFF000040;
  #if flash
  inline public static var HUNT_KNIGHT:String = "assets/mus/the-hunting-knight.mp3";
  #else
  inline public static var HUNT_KNIGHT:String = "assets/mus/the-hunting-knight.ogg";
  #end
  
  public static var title_music:Bool = false;
  
  private var title:FlxText;
  private var sub:FlxText;
  private var cs:FlxSpriteGroup;
  
  override public function create():Void {
    FlxG.cameras.bgColor = MENU_BGC;
    
    if(!title_music) {
      FlxG.sound.playMusic(HUNT_KNIGHT);
      title_music = true;
    }
    
    cs = new FlxSpriteGroup(3);
    
    var ypos:Int = 0;
    cs.add(title = new FlxText(0, ypos, 100, "Wiped"));
    ypos += 12;
    cs.add(sub = new FlxText(0, ypos, 100, "created by Pitzik4").setFormat("assets/fnt/The-6th-Point.ttf"));
    ypos += 20;
    title.alignment = sub.alignment = "center";
    var button:FlxButton = new FlxButton(10, ypos, "Play", function():Void {
      FlxG.switchState(new PlayState());
    });
    cs.add(button);
    ypos += 20;
    button = new FlxButton(10, ypos, "Settings", function():Void {
      FlxG.switchState(new SettingsState());
    });
    cs.add(button);
    ypos += 20;
    button = new FlxButton(10, ypos, "Info", function():Void {
      FlxG.switchState(new InfoState());
    });
    cs.add(button);
    ypos += 20;
    #if desktop
    button = new FlxButton(10, ypos, "Quit", function():Void {
      System.exit(0);
    });
    cs.add(button);
    ypos += 20;
    #end
    
    add(cs);
    
    add(new CurSprite());
    onResize(FlxG.stage.stageWidth, FlxG.stage.stageHeight);
  }
  override public function onResize(width:Int, height:Int):Void {
    super.onResize(width, height);
    width = cast width/FlxG.camera.zoom;
    height = cast height/FlxG.camera.zoom;
    cs.x = width/2-50;
    cs.y = height/2-40;
  }
}
