package pitzik4.games.ld48.ld28;

import flixel.FlxState;
import flixel.FlxG;
import flixel.group.FlxSpriteGroup;
import flixel.text.FlxText;
import flixel.ui.FlxButton;
import flixel.FlxSprite;

class InfoState extends FlxState {
  private var title:FlxText;
  private var body:FlxText;
  private var button:FlxButton;
  
  override public function create():Void {
    FlxG.cameras.bgColor = TitleState.MENU_BGC;
    
    title = new FlxText(0,8,Main.S_WIDTH,"Info");
    title.alignment = "center";
    
    body = new FlxText(8,24,Main.S_WIDTH-16,"This game was made by Pitzik4 in 2013 for the 28th triannual Ludum Dare 48-hour game jam. The theme was \"You Only Get One\".");
    
    add(title);
    add(body);
    
    button = new FlxButton(0, 0, "Back to Title", function():Void {
      FlxG.switchState(new TitleState());
    });
    add(button);
    
    add(new CurSprite());
    onResize(FlxG.stage.stageWidth, FlxG.stage.stageHeight);
  }
  override public function onResize(width:Int, height:Int):Void {
    super.onResize(width, height);
    width = cast width/FlxG.camera.zoom;
    height = cast height/FlxG.camera.zoom;
    title.width = width;
    body.width = width-16;
    button.y = height - button.height - 4;
    button.x = (width - button.width)*0.5;
  }
}
