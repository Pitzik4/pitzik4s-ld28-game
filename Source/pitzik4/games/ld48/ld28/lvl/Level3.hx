package pitzik4.games.ld48.ld28.lvl;

import flixel.FlxG;
import flixel.FlxSprite;
import flixel.FlxObject;
import flixel.util.FlxTimer;
import pitzik4.games.ld48.ld28.TriggerZone;
import pitzik4.games.ld48.ld28.boss.Monorail;
import pitzik4.games.ld48.ld28.char.DeadBad;

class Level3 extends Level {
  inline public static var lvl3:String = "assets/lvl/lvl3.csv";
  inline public static var SndTele:String = "assets/snd/teleport.wav";
  
  #if flash
  inline public static var BLAZE_SHIELD:String = "assets/mus/a-blazed-shield.mp3";
  #else
  inline public static var BLAZE_SHIELD:String = "assets/mus/a-blazed-shield.ogg";
  #end
  
  public var mono:Monorail;
  public var diaprog:Int = 0;
  public var lever:FlxSprite;
  public var diatime:Float = 0;
  public var door:FlxSprite;
  
  public function new(owner:PlayState) {
    super(lvl3,owner);
    var edoor:FlxSprite = new FlxSprite(12*32,10*32);
    edoor.loadGraphic(Level1.ImgDoodads, true, false, 32, 32);
    edoor.animation.add("closed", [3]);
    edoor.animation.add("open", [4]);
    edoor.animation.play("closed");
    edoor.immovable = true;
    edoor.allowCollisions = FlxObject.NONE;
    add(edoor);
    
    mono = new Monorail(25*32, 6*32, owner, 13*32);
    add(mono);
    
    var trig:TriggerZone = new TriggerZone(owner.player,13,8,1,3,function():Void {
      #if SKIP_FIRST_CUTSCENE
      diaprog = 9;
      #else
      FlxG.sound.music.stop();
      owner.lockon = mono.mrbad;
      owner.player.mobile = false;
      owner.player.velocity.set();
      owner.player.acceleration.x = 0;
      owner.dialog.str = "Hey! You! What are you doing here?";
      owner.dialog.icon = 3;
      owner.dialog.displayIcon = true;
      owner.indialog = true;
      diaprog = 1;
      #end
      });
    add(trig);
    
    lever = new FlxSprite(30*32,3*32);
    //lever = new FlxSprite(15*32,10*32);
    lever.loadGraphic(Level1.ImgDoodads, true);
    lever.animation.add("off", [5]);
    lever.animation.add("on", [6]);
    lever.animation.play("off");
    lever.allowCollisions = FlxObject.NONE;
    add(lever);
    trig = new TriggerZone(owner.player,30,3,1,1,function():Void {
      lever.animation.play("on");
      FlxG.sound.play(Level1.SndOpen);
      mono.stop(function():Void {
        diaprog = 64;
        });
      });
    add(trig);
    
    door = new FlxSprite(10*32,3*32);
    door.loadGraphic(Level1.ImgDoodads, true, false, 32, 32);
    door.animation.add("closed", [3]);
    door.animation.add("open", [4]);
    door.animation.play("closed");
    door.immovable = true;
    door.allowCollisions = FlxObject.NONE;
    add(door);
    trig = new TriggerZone(owner.player,10,3,1,1,function():Void {
      door.animation.play("open");
      FlxG.sound.play(Level1.SndOpen);
      FlxG.camera.fade(0xFF000000, 1.5, false, function():Void {
        FlxG.camera.fade(0xFF000000, 1.5, true, null, true);
        owner.switchLevel(new Level4(owner));
        destroy();
        });
      });
    add(trig);
  }
  override public function begin():Void {
    owner.player.setPosition(12*32,10*32);
    owner.add(mono);
    mono.active = false;
  }
  override public function end():Void {
    owner.remove(mono);
  }
  override public function update():Void {
    mono.active = true;
    super.update();
    mono.active = false;
    if(FlxG.keyboard.pressed('ENTER')) {
      owner.indialog = false;
      owner.diapause = false;
      owner.dialog.clear();
      owner.player.mobile = true;
      owner.lockon = null;
      if(diaprog > 0 && diaprog < 64) {
        owner.player.mobile = true;
        mono.start();
        FlxG.sound.playMusic(BLAZE_SHIELD);
        diaprog = 10;
      } else if(diaprog >= 64) {
        mono.remove(mono.mrbad);
        FlxG.sound.playMusic(TitleState.HUNT_KNIGHT);
        diaprog = 75;
        var trig:TriggerZone = new TriggerZone(owner.player,25,6,2,1,function():Void {
          mono.dropping = false;
          mono.start();
          var trig:TriggerZone = new TriggerZone(owner.player,20,6,2,1,function():Void {
            owner.lockon = null;
            owner.dialog.str = "This is fast! I'll have to get off quickly.";
            owner.dialog.icon = 1;
            owner.dialog.displayIcon = true;
            owner.indialog = true;
            owner.diapause = true;
            });
          add(trig);
          });
        add(trig);
      }
    }
    if(diaprog > 0 && !owner.indialog) {
      if(diaprog == 1) {
        owner.dialog.str = "Oh, hello, blue fellow. Um, could you give me a moment to...";
        owner.dialog.icon = 2;
        owner.dialog.displayIcon = true;
        owner.indialog = true;
        diaprog = 2;
      } else if(diaprog == 2) {
        owner.lockon = null;
        owner.dialog.str = "This room is strictly off-limits!";
        owner.dialog.icon = 3;
        owner.dialog.displayIcon = true;
        owner.indialog = true;
        diaprog = 3;
      } else if(diaprog == 3) {
        owner.dialog.str = "That's a strange one. How did I know that?";
        owner.dialog.displayIcon = false;
        owner.indialog = true;
        diaprog = 4;
      } else if(diaprog == 4) {
        owner.dialog.str = "I'm going to stop you there and turn you in.";
        owner.dialog.icon = 3;
        owner.dialog.displayIcon = true;
        owner.indialog = true;
        diaprog = 5;
      } else if(diaprog == 5) {
        owner.lockon = mono.mrbad;
        owner.dialog.str = "Oh, no, no, that's terrible. I can't let that happen. I really don't think I should try to talk to you at this point, so I'll just kill you by dropping things on your head. Yes, that sounds much better.";
        owner.dialog.icon = 2;
        owner.dialog.displayIcon = true;
        owner.indialog = true;
        diaprog = 6;
      } else if(diaprog == 6) {
        owner.lockon = null;
        owner.dialog.str = "Is that loaded with mini-reactor casings? That's not good. I'd better watch my step, one of those dropped on my head would probably kill me instantly.";
        owner.dialog.icon = 1;
        owner.dialog.displayIcon = true;
        owner.indialog = true;
        diaprog = 7;
      } else if(diaprog == 7) {
        owner.lockon = null;
        owner.dialog.str = "I see a lever in a small room to the upper-right of him. I'm pretty sure that lever will stop the lift moving so I can get a hold of him.";
        owner.dialog.icon = 1;
        owner.dialog.displayIcon = true;
        owner.indialog = true;
        diaprog = 8;
      } else if(diaprog == 8) {
        owner.lockon = null;
        owner.dialog.str = "...Or I could just shoot him...";
        owner.dialog.icon = 0;
        owner.dialog.displayIcon = true;
        owner.indialog = true;
        diaprog = 9;
      } else if(diaprog == 9) {
        owner.player.mobile = true;
        mono.start();
        FlxG.sound.playMusic(BLAZE_SHIELD);
        diaprog = 10;
      } else if(diaprog == 64) {
        FlxG.sound.music.stop();
        owner.player.mobile = false;
        owner.player.velocity.set();
        owner.player.acceleration.x = 0;
        owner.lockon = mono.mrbad;
        owner.dialog.str = "Well, that went abysmally.";
        owner.dialog.icon = 2;
        owner.dialog.displayIcon = true;
        owner.indialog = true;
        diaprog = 65;
      } else if(diaprog == 65) {
        owner.lockon = null;
        owner.dialog.str = "Come with me. You're under arrest.";
        owner.dialog.icon = 3;
        owner.dialog.displayIcon = true;
        owner.indialog = true;
        diaprog = 66;
      } else if(diaprog == 66) {
        owner.lockon = mono.mrbad;
        owner.dialog.str = "Where do you plan on taking me? To the station? I don't think you have anywhere to go.";
        owner.dialog.icon = 2;
        owner.dialog.displayIcon = true;
        owner.indialog = true;
        diaprog = 67;
      } else if(diaprog == 67) {
        owner.lockon = mono.mrbad;
        owner.dialog.str = "Rather than stay here and wait indefinitely to be picked up and brought to jail - or possibly even get shot - I'm going to leave. It looks like I'll have to get by with you on my tail. To make that easier for me, would you mind not threatening me next time you see me?";
        owner.dialog.icon = 2;
        owner.dialog.displayIcon = true;
        owner.indialog = true;
        diaprog = 68;
      } else if(diaprog == 68) {
        owner.lockon = null;
        owner.dialog.str = "Nonsense! You have nowhere to go!";
        owner.dialog.icon = 3;
        owner.dialog.displayIcon = true;
        owner.indialog = true;
        diaprog = 69;
      } else if(diaprog == 69) {
        owner.lockon = mono.mrbad;
        owner.dialog.str = "On the contrary - allow me to demonstrate.";
        owner.dialog.icon = 2;
        owner.dialog.displayIcon = true;
        owner.indialog = true;
        diaprog = 70;
      } else if(diaprog == 70) {
        mono.mrbad.animation.play("teleport");
        FlxG.sound.play(SndTele);
        diaprog = 71;
      } else if(diaprog == 71) {
        diatime += FlxG.elapsed;
        if(diatime >= 1.5) {
          diaprog = 72;
        }
      } else if(diaprog == 72) {
        mono.remove(mono.mrbad);
        owner.lockon = null;
        owner.dialog.str = "A teleporter? That's really annoying.";
        owner.dialog.icon = 0;
        owner.dialog.displayIcon = true;
        owner.indialog = true;
        diaprog = 73;
      } else if(diaprog == 73) {
        owner.lockon = null;
        owner.dialog.str = "I'll ride the lift to the other side and see what's there.";
        owner.dialog.icon = 1;
        owner.dialog.displayIcon = true;
        owner.indialog = true;
        diaprog = 74;
      } else if(diaprog == 74) {
        FlxG.sound.playMusic(TitleState.HUNT_KNIGHT);
        owner.player.mobile = true;
        diaprog = 75;
        var trig:TriggerZone = new TriggerZone(owner.player,25,6,2,1,function():Void {
          mono.dropping = false;
          mono.start();
          var trig:TriggerZone = new TriggerZone(owner.player,20,6,2,1,function():Void {
            owner.lockon = null;
            owner.dialog.str = "This is fast! I'll have to get off quickly.";
            owner.dialog.icon = 1;
            owner.dialog.displayIcon = true;
            owner.indialog = true;
            owner.diapause = true;
            });
          add(trig);
          });
        add(trig);
      } else if(diaprog == 256) {
          FlxG.sound.music.stop();
          owner.player.mobile = false;
          owner.player.velocity.set();
          owner.player.acceleration.x = 0;
          owner.dialog.str = "You shouldn't have tried to kill me.";
          owner.dialog.icon = 1;
          owner.dialog.displayIcon = true;
          owner.indialog = true;
          diaprog = 257;
        } else if(diaprog == 257) {
          owner.dialog.str = "I waited there for something. I knew somebody was supposed take me somewhere at this point.";
          owner.dialog.displayIcon = false;
          owner.indialog = true;
          diaprog = 258;
        } else if(diaprog == 258) {
          owner.dialog.str = "I waited and waited.";
          owner.dialog.displayIcon = false;
          owner.indialog = true;
          diaprog = 259;
        } else if(diaprog == 259) {
          owner.player.superanim = "liedown";
          owner.dialog.str = "And waited.";
          owner.dialog.displayIcon = false;
          owner.indialog = true;
          owner.end();
          diaprog = 260;
        }
    }
  }
  override public function destroy():Void {
    super.destroy();
    mono = null;
  }
  override public function fresh():Level {
    return new Level3(owner);
  }
  override public function shoot():Bool {
    if(diaprog > 0) {
      owner.indialog = false;
      owner.diapause = false;
      owner.dialog.clear();
      var dbad:DeadBad = new DeadBad(mono.mrbad.x, mono.mrbad.y, mono.mrbad.x > owner.player.x ? FlxObject.LEFT : FlxObject.RIGHT);
      add(dbad);
      mono.going = false;
      mono.velocity = 0;
      mono.remove(mono.mrbad);
      owner.player.mobile = false;
      owner.player.acceleration.x = 0;
      owner.player.velocity.set();
      owner.lockon = dbad;
      diaprog = 255;
      FlxTimer.start(2, function(timer:FlxTimer):Void {
        owner.lockon = null;
        diaprog = 256;
        });
      FlxG.sound.music.stop();
      return true;
    }
    return false;
  }
}
