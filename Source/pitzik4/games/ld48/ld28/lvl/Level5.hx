package pitzik4.games.ld48.ld28.lvl;

import flixel.FlxG;
import flixel.FlxSprite;
import flixel.FlxObject;
import pitzik4.games.ld48.ld28.TriggerZone;
import pitzik4.games.ld48.ld28.ClosingDoor;
import pitzik4.games.ld48.ld28.char.DeadBad;

class Level5 extends Level {
  inline public static var lvl5:String = "assets/lvl/lvl5.csv";
  inline public static var ImgTrans:String = "assets/gfx/transmitter.png";
  
  public var mrbad:FlxSprite;
  public var diaprog:Int = 0;
  
  public var halldone:Bool = false;
  public var stoptime:Float = 0;
  public var fdiaprog:Int = 0;
  
  public var timeleft:Float = 10;
  
  public var ending:Int = 0;
  public var endtime:Float = 0;
  
  public function new(owner:PlayState) {
    super(lvl5,owner);
    
    var trig:TriggerZone;
    
    var edoor:FlxSprite = new FlxSprite(8*32,10*32);
    edoor.loadGraphic(Level1.ImgDoodads, true, false, 32, 32);
    edoor.animation.add("closed", [3]);
    edoor.animation.add("open", [4]);
    edoor.animation.play("closed");
    edoor.immovable = true;
    edoor.allowCollisions = FlxObject.NONE;
    add(edoor);
    
    var transmitter:FlxSprite = new FlxSprite(41*32,7*32,ImgTrans);
    transmitter.allowCollisions = FlxObject.NONE;
    add(transmitter);
    
    mrbad = new FlxSprite(12*32,10*32);
    mrbad.loadGraphic(Level1.ImgMrBad, true, true, 32, 32);
    mrbad.animation.add("inspect", [5, 6, 6, 5, 5, 6], 1);
    mrbad.animation.add("uhoh", [7]);
    mrbad.animation.add("stand", [0]);
    mrbad.animation.add("walk", [1,0,2,0], 6);
    mrbad.animation.add("teleport", [8,8,8,8,9,10,11,12,13,14,15,18], 10, false);
    mrbad.animation.play("inspect");
    mrbad.allowCollisions = FlxObject.NONE;
    mrbad.facing = FlxObject.RIGHT;
    add(mrbad);
    
    trig = new TriggerZone(owner.player,10,8,1,3,function():Void {
      mrbad.animation.play("uhoh");
      FlxG.sound.music.stop();
      owner.player.mobile = false;
      owner.player.velocity.set();
      owner.player.acceleration.x = 0;
      owner.dialog.str = "You again? No big surprise there, really, but my teleporter needs to be recharged, so I guess I'll just... run.";
      owner.dialog.icon = 2;
      owner.dialog.displayIcon = true;
      owner.indialog = true;
      diaprog = 1;
      });
    add(trig);
  }
  override public function begin():Void {
    owner.player.setPosition(8*32,10*32);
  }
  @:access(flixel.FlxCamera)
  override public function update():Void {
    super.update();
    if(owner.player.acceleration.x > 0 || diaprog < 4 || halldone) {
      stoptime = 0;
    } else {
      stoptime += FlxG.elapsed;
      if(stoptime >= 0.5) {
        mrbad.velocity.set();
        mrbad.facing = FlxObject.LEFT;
        mrbad.animation.play("stand");
        halldone = true;
        fdiaprog = diaprog;
        diaprog = 64;
        owner.player.mobile = false;
        owner.player.velocity.set();
        owner.player.acceleration.x = 0;
      }
    }
    if(FlxG.keyboard.pressed('ENTER')) {
      if(diaprog >= 64 && ending == 0) {
        owner.indialog = false;
        owner.diapause = false;
        owner.dialog.clear();
        owner.player.mobile = true;
        owner.player.superanim = null;
        if(diaprog < 79) {
          mrbad.animation.play("walk");
          mrbad.facing = FlxObject.RIGHT;
          mrbad.velocity.x = 58;
          diaprog = 78;
        } else {
          diaprog = 86;
        }
      }
    }
    while(!halldone && owner.player.x > 29*32) {
      owner.player.x -= 64;
      mrbad.x -= 64;
      FlxG.camera.scroll.x -= 64;
      FlxG.camera._scrollTarget.x -= 64;
    }
    if(ending == 0 && diaprog > 0 && !owner.indialog) {
      if(diaprog == 1) {
        mrbad.animation.play("stand");
        owner.dialog.str = "Stop!";
        owner.dialog.icon = 3;
        owner.dialog.displayIcon = true;
        owner.indialog = true;
        diaprog = 2;
      } else if(diaprog == 2) {
        mrbad.animation.play("stand");
        owner.dialog.str = "I chased him while we each tried to persuade the other to stop.\n(You'll be able to run after him when this dialog disappears.)";
        owner.indialog = true;
        diaprog = 3;
      } else if(diaprog == 3) {
        owner.player.mobile = true;
        mrbad.animation.play("walk");
        mrbad.velocity.x = 58;
        owner.dialog.str = "Come back now!";
        owner.dialog.icon = 3;
        owner.dialog.displayIcon = true;
        owner.indialog = true;
        diaprog = 4;
      } else if(diaprog == 4) {
        owner.dialog.str = "No, you have to listen to me!";
        owner.dialog.icon = 2;
        owner.dialog.displayIcon = true;
        owner.indialog = true;
        diaprog = 5;
      } else if(diaprog == 5) {
        owner.dialog.str = "I don't think there's anything you can say to defend yourself!";
        owner.dialog.icon = 3;
        owner.dialog.displayIcon = true;
        owner.indialog = true;
        diaprog = 6;
      } else if(diaprog == 6) {
        owner.dialog.str = "Do you even know why you want to catch me so much? It's because you've been brainwashed!";
        owner.dialog.icon = 2;
        owner.dialog.displayIcon = true;
        owner.indialog = true;
        diaprog = 7;
      } else if(diaprog == 7) {
        owner.dialog.str = "You're an alien! They don't tell you that, but you are! Your race was enslaved by the humans and made into our worldwide police force!";
        owner.dialog.icon = 2;
        owner.dialog.displayIcon = true;
        owner.indialog = true;
        diaprog = 8;
      } else if(diaprog == 8) {
        owner.dialog.str = "That's ridiculous! Aliens don't exist!";
        owner.dialog.icon = 3;
        owner.dialog.displayIcon = true;
        owner.indialog = true;
        diaprog = 9;
      } else if(diaprog == 9) {
        owner.dialog.str = "How do you explain the fact that all cops have blue skin and nobody else does, then?";
        owner.dialog.icon = 2;
        owner.dialog.displayIcon = true;
        owner.indialog = true;
        diaprog = 10;
      } else if(diaprog == 10) {
        owner.dialog.str = "I don't even know what a \"cop\" is!";
        owner.dialog.icon = 3;
        owner.dialog.displayIcon = true;
        owner.indialog = true;
        diaprog = 11;
      } else if(diaprog == 11) {
        owner.dialog.str = "What? Oh, they must have wiped your memory so I would have a harder time persuading you...";
        owner.dialog.icon = 2;
        owner.dialog.displayIcon = true;
        owner.indialog = true;
        diaprog = 12;
      } else if(diaprog == 12) {
        owner.dialog.str = "Look, could you please just stop so I can explain things to you?";
        owner.dialog.icon = 2;
        owner.dialog.displayIcon = true;
        owner.indialog = true;
        diaprog = 13;
      } else if(diaprog == 13) {
        owner.dialog.str = "It's you who is required by law to stop right now, not me.";
        owner.dialog.icon = 3;
        owner.dialog.displayIcon = true;
        owner.indialog = true;
        diaprog = 14;
      } else if(diaprog == 14) {
        owner.dialog.str = "In fact, if you don't stop running, I'm authorized to shoot you.";
        owner.dialog.icon = 3;
        owner.dialog.displayIcon = true;
        owner.indialog = true;
        diaprog = 15;
      } else if(diaprog == 15) {
        owner.player.acceleration.x = 0;
        owner.player.velocity.set();
        owner.player.mobile = false;
      } else if(diaprog == 64) {
        owner.dialog.str = "You stopped. That's good. Now, let me explain the way things are.";
        owner.dialog.icon = 2;
        owner.dialog.displayIcon = true;
        owner.indialog = true;
        if(fdiaprog < 8) {
          diaprog = 66;
        } else if(fdiaprog < 12) {
          diaprog = 67;
        } else {
          diaprog = 69;
        }
      } else if(diaprog == 66) {
        owner.dialog.str = "You're from a race of aliens that was brainwashed and enslaved by the human race to become a worldwide police force.";
        owner.dialog.icon = 2;
        owner.dialog.displayIcon = true;
        owner.indialog = true;
        diaprog = 67;
      } else if(diaprog == 67) {
        owner.dialog.str = "I'm starting to notice... I don't remember anything.";
        owner.dialog.icon = 1;
        owner.dialog.displayIcon = true;
        owner.indialog = true;
        diaprog = 68;
      } else if(diaprog == 68) {
        owner.dialog.str = "Hmm, so they wiped your memory. That would be a measure to help keep me from persuading you to help me, I suppose. It didn't work, though. Good job.";
        owner.dialog.icon = 2;
        owner.dialog.displayIcon = true;
        owner.indialog = true;
        diaprog = 69;
      } else if(diaprog == 69) {
        owner.dialog.str = "So, put simply, I'm trying to free your race.";
        owner.dialog.icon = 2;
        owner.dialog.displayIcon = true;
        owner.indialog = true;
        diaprog = 70;
      } else if(diaprog == 70) {
        owner.dialog.str = "Why did you try to kill me earlier, then?";
        owner.dialog.icon = 1;
        owner.dialog.displayIcon = true;
        owner.indialog = true;
        diaprog = 71;
      } else if(diaprog == 71) {
        owner.dialog.str = "I barely survived that!";
        owner.dialog.icon = 3;
        owner.dialog.displayIcon = true;
        owner.indialog = true;
        diaprog = 72;
      } else if(diaprog == 72) {
        owner.dialog.str = "Alright, you got me. I'm trying to free your race, but that doesn't make me a good guy.";
        owner.dialog.icon = 2;
        owner.dialog.displayIcon = true;
        owner.indialog = true;
        diaprog = 73;
      } else if(diaprog == 73) {
        owner.dialog.str = "With you gone, there will be no law enforcement until you're replaced. I work for a criminal group, and I'll be receiving a large payment for this.";
        owner.dialog.icon = 2;
        owner.dialog.displayIcon = true;
        owner.indialog = true;
        diaprog = 74;
      } else if(diaprog == 74) {
        owner.dialog.str = "Freedom for you, freedom for us. Your help would be greatly appreciated.";
        owner.dialog.icon = 2;
        owner.dialog.displayIcon = true;
        owner.indialog = true;
        diaprog = 75;
      } else if(diaprog == 75) {
        owner.dialog.str = "I don't like you, but I'll help you. What do you need?";
        owner.dialog.icon = 1;
        owner.dialog.displayIcon = true;
        owner.indialog = true;
        diaprog = 76;
      } else if(diaprog == 76) {
        owner.dialog.str = "Follow me to the brainwave transmitter.";
        owner.dialog.icon = 2;
        owner.dialog.displayIcon = true;
        owner.indialog = true;
        diaprog = 77;
      } else if(diaprog == 77) {
        owner.player.mobile = true;
        mrbad.animation.play("walk");
        mrbad.facing = FlxObject.RIGHT;
        mrbad.velocity.x = 58;
        diaprog = 78;
      } else if(diaprog == 79) {
        owner.dialog.str = "I've been trying to hack into it and disable it, but that's very difficult.";
        owner.dialog.icon = 2;
        owner.dialog.displayIcon = true;
        owner.indialog = true;
        diaprog = 80;
      } else if(diaprog == 80) {
        owner.dialog.str = "The people in power really trust you guys - probably too much considering how easily I got you to help me.";
        owner.dialog.icon = 2;
        owner.dialog.displayIcon = true;
        owner.indialog = true;
        diaprog = 81;
      } else if(diaprog == 81) {
        owner.dialog.str = "Shooting into this machine using a police-issued weapon will turn it off. They added that feature just in case.";
        owner.dialog.icon = 2;
        owner.dialog.displayIcon = true;
        owner.indialog = true;
        diaprog = 82;
      } else if(diaprog == 82) {
        owner.dialog.str = "Your people will immediately realize their situation. Chances are, a lot of them will lash out at random people.";
        owner.dialog.icon = 2;
        owner.dialog.displayIcon = true;
        owner.indialog = true;
        diaprog = 83;
      } else if(diaprog == 83) {
        owner.dialog.str = "We've built a lesser version of this machine. It will implant a suggestion into their minds to go to a certain spot, where he have a spaceship prepared that will bring them back to their home planet.";
        owner.dialog.icon = 2;
        owner.dialog.displayIcon = true;
        owner.indialog = true;
        diaprog = 84;
      } else if(diaprog == 84) {
        owner.dialog.str = "I can charge my teleporter using a socket on this machine. We'll both teleport out, I'll send you to the spaceship. You can return to your planet. Chaos will ensue on ours, but that will work out fine for me.";
        owner.dialog.icon = 2;
        owner.dialog.displayIcon = true;
        owner.indialog = true;
        diaprog = 85;
      } else if(diaprog == 85) {
        owner.dialog.str = "I triggered a few alarms on the way here, so I'd say you have about 10 seconds to shoot it. Then again, you might get a promotion or something if you don't. Make your decision.";
        owner.dialog.icon = 2;
        owner.dialog.displayIcon = true;
        owner.indialog = true;
        diaprog = 86;
        owner.player.mobile = true;
      } else if(diaprog == 86) {
        timeleft -= FlxG.elapsed;
        if(timeleft <= 0) {
          ending = 1;
          diaprog = 0;
        }
      }
    } else if(!owner.indialog) {
      if(ending == 1) {
        if(diaprog == 0) {
          FlxG.sound.music.stop();
          owner.player.mobile = false;
          owner.player.velocity.set();
          owner.player.acceleration.x = 0;
          owner.dialog.str = "I really thought I had you on my side there. How disappointing.";
          owner.dialog.icon = 2;
          owner.dialog.displayIcon = true;
          owner.indialog = true;
          diaprog = 1;
        } else if(diaprog == 1) {
          owner.dialog.str = "I'll be teleported to prison in seconds. I hope you -";
          owner.dialog.icon = 2;
          owner.dialog.displayIcon = true;
          owner.indialog = true;
          diaprog = 2;
        } else if(diaprog == 2) {
          mrbad.animation.play("teleport");
          endtime = 1.5;
          diaprog = 3;
        } else if(diaprog == 3) {
          endtime -= FlxG.elapsed;
          if(endtime <= 0) {
            diaprog = 4;
          }
        } else if(diaprog == 4) {
          owner.dialog.str = "I hope I was right...";
          owner.dialog.icon = 1;
          owner.dialog.displayIcon = true;
          owner.indialog = true;
          diaprog = 5;
        } else if(diaprog == 5) {
          owner.player.superanim = "teleport";
          endtime = 2;
          diaprog = 6;
        } else if(diaprog == 6) {
          endtime -= FlxG.elapsed;
          if(endtime <= 0) {
            diaprog = 7;
          }
        } else if(diaprog == 7) {
          owner.dialog.str = "I was wrong. I wasn't promoted. I had learned too much and come too close to helping a wanted criminal. I was teleported into a high-security prison, and I don't think I'm ever getting out.";
          owner.indialog = true;
          if(owner.bullet) {
            diaprog = 8;
          } else {
            diaprog = 9;
          }
        } else if(diaprog == 8) {
          owner.dialog.str = "I still have one bullet left in my gun, though.";
          owner.indialog = true;
          for(i in _members) {
            remove(i);
          }
          var gun = new FlxSprite(0,0,"assets/gfx/gun.png");
          gun.scrollFactor.set();
          owner.remove(owner.player);
          add(gun);
          diaprog = 9;
        } else if(diaprog == 9) {
          owner.end();
          diaprog = 10;
        }
      } else if(ending == 2) {
        if(diaprog == 0) {
          FlxG.sound.music.stop();
          owner.player.mobile = false;
          owner.player.velocity.set();
          owner.player.acceleration.x = 0;
          owner.dialog.str = "Thanks.";
          owner.dialog.icon = 2;
          owner.dialog.displayIcon = true;
          owner.indialog = true;
          diaprog = 1;
        } else if(diaprog == 1) {
          owner.dialog.str = "But I lied. This teleporter can't hold two charges.";
          owner.dialog.icon = 2;
          owner.dialog.displayIcon = true;
          owner.indialog = true;
          diaprog = 2;
        } else if(diaprog == 2) {
          owner.dialog.str = "I also don't know why you believed that would waste our money on a spaceship. Most of your kind are just going to be killed, the rest re-enslaved.";
          owner.dialog.icon = 2;
          owner.dialog.displayIcon = true;
          owner.indialog = true;
          diaprog = 3;
        } else if(diaprog == 3) {
          owner.dialog.str = "On the bright side, though, I'm getting paid. Well, goodbye! I doubt that anybody will bother to come pick you up and bring you back to civilization after what you just did.";
          owner.dialog.icon = 2;
          owner.dialog.displayIcon = true;
          owner.indialog = true;
          diaprog = 4;
        } else if(diaprog == 4) {
          mrbad.animation.play("teleport");
          endtime = 1.5;
          diaprog = 5;
        } else if(diaprog == 5) {
          endtime -= FlxG.elapsed;
          if(endtime <= 0) {
            diaprog = 6;
          }
        } else if(diaprog == 6) {
          owner.dialog.str = "...";
          owner.dialog.icon = 0;
          owner.dialog.displayIcon = true;
          owner.indialog = true;
          diaprog = 7;
        } else if(diaprog == 7) {
          owner.end();
          diaprog = 8;
        }
      } else if(ending == 3) {
        if(diaprog == 0) {
          FlxG.sound.music.stop();
          owner.player.mobile = false;
          owner.player.velocity.set();
          owner.player.acceleration.x = 0;
          owner.dialog.str = "You shouldn't have tried to kill me.";
          owner.dialog.icon = 0;
          owner.dialog.displayIcon = true;
          owner.indialog = true;
          diaprog = 1;
        } else if(diaprog == 1) {
          owner.dialog.str = "I waited there for something. I knew somebody was supposed take me somewhere at this point.";
          owner.dialog.displayIcon = false;
          owner.indialog = true;
          diaprog = 2;
        } else if(diaprog == 2) {
          owner.dialog.str = "I waited and waited.";
          owner.dialog.displayIcon = false;
          owner.indialog = true;
          diaprog = 3;
        } else if(diaprog == 3) {
          owner.player.superanim = "liedown";
          owner.dialog.str = "And waited.";
          owner.dialog.displayIcon = false;
          owner.indialog = true;
          owner.end();
          diaprog = 5;
        }
      }
    }
    if(diaprog == 78) {
      if(mrbad.x > 43*32) {
        mrbad.x = 43*32;
        mrbad.velocity.set();
        mrbad.animation.play("stand");
        mrbad.facing = FlxObject.LEFT;
      }
      if(owner.player.x >= 41*32) {
        owner.player.mobile = false;
        owner.player.acceleration.x = 0;
        owner.player.velocity.x = 0;
        diaprog = 79;
      }
    } else if(diaprog > 78) {
      if(owner.player.x > mrbad.x) {
        mrbad.facing = FlxObject.RIGHT;
      } else {
        mrbad.facing = FlxObject.LEFT;
      }
    }
  }
  override public function destroy():Void {
    super.destroy();
  }
  override public function fresh():Level {
    return new Level5(owner);
  }
  override public function shoot():Bool {
    if(diaprog >= 81) {
      ending = 2;
      diaprog = 0;
    } else {
      ending = 3;
      diaprog = 0;
      owner.dialog.clear();
      owner.player.mobile = false;
      owner.player.acceleration.x = 0;
      owner.player.velocity.set();
      add(new DeadBad(mrbad.x, mrbad.y));
      remove(mrbad);
      FlxG.sound.music.stop();
    }
    owner.indialog = owner.diapause = false;
    return true;
  }
}
