package pitzik4.games.ld48.ld28.lvl;

import flixel.FlxG;
import flixel.FlxSprite;
import flixel.FlxObject;
import pitzik4.games.ld48.ld28.TriggerZone;
import pitzik4.games.ld48.ld28.ClosingDoor;

class Level4 extends Level {
  inline public static var lvl4:String = "assets/lvl/lvl4.csv";
  
  public var door:FlxSprite;
  
  public function new(owner:PlayState) {
    super(lvl4,owner);
    
    var trig:TriggerZone;
    
    var edoor:FlxSprite = new FlxSprite(16*32,20*32);
    edoor.loadGraphic(Level1.ImgDoodads, true, false, 32, 32);
    edoor.animation.add("closed", [3]);
    edoor.animation.add("open", [4]);
    edoor.animation.play("closed");
    edoor.immovable = true;
    edoor.allowCollisions = FlxObject.NONE;
    add(edoor);
    
    var lever:FlxSprite = new FlxSprite(23*32,11*32);
    lever.loadGraphic(Level1.ImgDoodads, true);
    lever.animation.add("off", [5]);
    lever.animation.add("on", [6]);
    lever.animation.play("off");
    lever.allowCollisions = FlxObject.NONE;
    add(lever);
    trig = new TriggerZone(owner.player,23,11,1,1,function():Void {
      lever.animation.play("on");
      FlxG.sound.play(Level1.SndOpen);
      FlxG.camera.shake(0.0025, 0.1);
      tiles.setTile(11,11,11);
      tiles.setTile(12,11,12);
      var cldoor:ClosingDoor = new ClosingDoor(22*32,2*32,2*32,2*64,30);
      add(cldoor);
      owner.dialog.str = "I should be quick, the door's closing.";
      owner.dialog.icon = 1;
      owner.dialog.displayIcon = true;
      owner.indialog = true;
      owner.diapause = true;
      });
    add(trig);
    
    door = new FlxSprite(23*32,5*32);
    door.loadGraphic(Level1.ImgDoodads, true, false, 32, 32);
    door.animation.add("closed", [3]);
    door.animation.add("open", [4]);
    door.animation.play("closed");
    door.immovable = true;
    door.allowCollisions = FlxObject.NONE;
    add(door);
    trig = new TriggerZone(owner.player,23,5,1,1,function():Void {
      door.animation.play("open");
      FlxG.sound.play(Level1.SndOpen);
      FlxG.camera.fade(0xFF000000, 1.5, false, function():Void {
        FlxG.camera.fade(0xFF000000, 1.5, true, null, true);
        owner.switchLevel(new Level5(owner));
        destroy();
        });
      });
    add(trig);
  }
  override public function begin():Void {
    owner.player.setPosition(16*32,20*32);
  }
  override public function update():Void {
    super.update();
    if(FlxG.keyboard.pressed('ENTER')) {
      owner.indialog = false;
      owner.diapause = false;
      owner.dialog.clear();
      owner.player.mobile = true;
      owner.player.superanim = null;
    }
  }
  override public function destroy():Void {
    super.destroy();
  }
  override public function fresh():Level {
    return new Level4(owner);
  }
}
