package pitzik4.games.ld48.ld28.lvl;

import flixel.FlxG;
import flixel.FlxSprite;
import flixel.FlxObject;
import flixel.util.FlxTimer;
import pitzik4.games.ld48.ld28.TriggerZone;
import pitzik4.games.ld48.ld28.char.DeadBad;

class Level1 extends Level {
  inline public static var lvl1:String = "assets/lvl/lvl1.csv";
  inline public static var ImgMrBad:String = "assets/gfx/mrbad.png";
  inline public static var ImgDoodads:String = "assets/gfx/doodads.png";
  inline public static var SndOpen:String = "assets/snd/open.wav";
  
  public var diaprog:Int = 0;
  public var mrbad:FlxSprite;
  public var twinkle:FlxSprite;
  public var door:FlxSprite;
  public var badtime:Float = -1;
  
  public function new(owner:PlayState) {
    super(lvl1,owner);
    var trig:TriggerZone = new TriggerZone(owner.player,22,10,1,3,function():Void {
      owner.dialog.str = "I'll take a look around.";
      owner.dialog.icon = 1;
      owner.dialog.displayIcon = true;
      owner.indialog = true;
      owner.diapause = true;
      });
    add(trig);
    
    door = new FlxSprite(57*32,15*32);
    door.loadGraphic(ImgDoodads, true, false, 32, 32);
    door.animation.add("closed", [3]);
    door.animation.add("open", [4]);
    door.animation.play("closed");
    door.immovable = true;
    door.allowCollisions = FlxObject.NONE;
    add(door);
    
    mrbad = new FlxSprite(51*32,15*32);
    mrbad.loadGraphic(ImgMrBad, true, false, 32, 32);
    mrbad.animation.add("inspect", [5, 6, 6, 5, 5, 6], 1);
    mrbad.animation.add("uhoh", [7]);
    mrbad.animation.add("stand", [0]);
    mrbad.animation.add("walk", [1,0,2,0], 6);
    mrbad.animation.play("inspect");
    add(mrbad);
    
    twinkle = new FlxSprite(51*32,15*32+3);
    twinkle.loadGraphic(ImgDoodads, true, false, 32, 32);
    twinkle.animation.add("twinkle", [0,0,0,1,0,0,1,2,2,1,0], 6);
    twinkle.animation.play("twinkle");
    twinkle.allowCollisions = FlxObject.NONE;
    
    trig = new TriggerZone(owner.player,48,13,1,3,function():Void {
      badtime = 0;
      diaprog = 64;
      owner.player.mobile = false;
      owner.player.velocity.set();
      owner.player.animation.play("idle");
      });
    add(trig);
    trig = new TriggerZone(owner.player,51,13,1,3,function():Void {
      owner.dialog.str = "Something's sparkling on the ground here.";
      owner.dialog.icon = 0;
      owner.dialog.displayIcon = true;
      owner.indialog = true;
      owner.diapause = true;
      diaprog = 96;
      });
    add(trig);
    
    trig = new TriggerZone(owner.player,57,15,1,1,function():Void {
      door.animation.play("open");
      FlxG.sound.play(SndOpen);
      FlxG.camera.fade(0xFF000000, 1.5, false, function():Void {
        FlxG.camera.fade(0xFF000000, 1.5, true, null, true);
        owner.switchLevel(new Level2(owner));
        destroy();
        });
      });
    add(trig);
  }
  override public function begin():Void {
    #if !SKIP_FIRST_CUTSCENE
    owner.player.mobile = false;
    owner.player.superanim = "liedown";
    owner.dialog.str = "One day, I woke up on the floor somewhere underground. I guess I was born there.\nC to advance text.\nEnter to skip cutscenes.";
    owner.dialog.displayIcon = false;
    owner.indialog = true;
    FlxG.sound.music.volume = 0;
    #else
    diaprog = 10;
    #end
    owner.player.setPosition(19*32,12*32);
  }
  override public function update():Void {
    super.update();
    if(diaprog <= 6) {
      if(FlxG.keyboard.pressed('ENTER')) {
        owner.indialog = false;
        owner.diapause = false;
        owner.player.mobile = true;
        owner.player.superanim = null;
        owner.dialog.clear();
        FlxG.sound.music.volume = 1;
        diaprog = 10;
      }
      if(!owner.indialog) {
        if(diaprog == 0) {
          owner.player.superanim = "wakeup";
          owner.dialog.str = "This didn't seem strange to me at all. I had never known anything else.";
          owner.dialog.displayIcon = false;
          owner.indialog = true;
        } else if(diaprog == 1) {
          owner.player.superanim = null;
          owner.dialog.str = "I got up and realized that I had a gun.";
          owner.dialog.displayIcon = false;
          owner.indialog = true;
        } else if(diaprog == 2) {
          owner.player.superanim = null;
          owner.dialog.str = "How many bullets...? Hmm... Looks like I only get one.";
          owner.dialog.icon = 0;
          owner.dialog.displayIcon = true;
          owner.indialog = true;
        } else if(diaprog == 3) {
          owner.player.superanim = null;
          owner.dialog.str = "I'd better use it wisely.";
          owner.dialog.icon = 1;
          owner.dialog.displayIcon = true;
          owner.indialog = true;
        } else if(diaprog == 4) {
          owner.player.superanim = null;
          owner.dialog.str = "Arrow keys or WASD to move, Z or space to jump, X or M to shoot, Esc to return to the title screen.";
          owner.dialog.displayIcon = false;
          owner.indialog = true;
        } else if(diaprog == 5) {
          owner.player.superanim = null;
          owner.dialog.str = "I'm a good shot, so don't worry about getting close to something before shooting it. I'll just shoot whatever seems logical.";
          owner.dialog.displayIcon = false;
          owner.indialog = true;
        } else if(diaprog == 6) {
          owner.player.mobile = true;
          FlxG.sound.music.volume = 1;
        }
        diaprog++;
      }
    } else if(diaprog >= 96 && (diaprog < 100 || diaprog >= 256)) {
      if(FlxG.keyboard.pressed('ENTER') && diaprog < 256) {
        owner.indialog = false;
        owner.diapause = false;
        owner.dialog.clear();
        owner.player.mobile = true;
        remove(twinkle);
        twinkle = null;
        diaprog = 100;
      }
      if(!owner.indialog) {
        if(diaprog == 96) {
          remove(twinkle);
          twinkle = null;
          owner.dialog.str = "It looks like a microchip.";
          owner.dialog.icon = 1;
          owner.dialog.displayIcon = true;
          owner.indialog = true;
          owner.diapause = true;
        } else if(diaprog == 97) {
          owner.dialog.str = "How did I know that? I didn't think to wonder.";
          owner.dialog.displayIcon = false;
          owner.indialog = true;
          owner.diapause = true;
        } else if(diaprog == 98) {
          owner.dialog.str = "I don't like the look of that guy. I think I should follow him.";
          owner.dialog.icon = 1;
          owner.dialog.displayIcon = true;
          owner.indialog = true;
          owner.diapause = true;
        } else if(diaprog == 256) {
          owner.dialog.str = "What did I do that for?";
          owner.dialog.icon = 1;
          owner.dialog.displayIcon = true;
          owner.indialog = true;
          owner.diapause = false;
        } else if(diaprog == 257) {
          owner.dialog.str = "I shot that guy... Just because he was in the same cave as me!";
          owner.dialog.icon = 1;
          owner.dialog.displayIcon = true;
          owner.indialog = true;
          owner.diapause = false;
        } else if(diaprog == 258) {
          owner.dialog.str = "All of the rules I had just broken flooded into my head. I don't know where they came from.";
          owner.dialog.displayIcon = false;
          owner.indialog = true;
          owner.diapause = false;
        } else if(diaprog == 259) {
          owner.dialog.str = "I knew I would receive punishment soon. There was nothing to do but wait for it.";
          owner.dialog.displayIcon = false;
          owner.indialog = true;
          owner.diapause = false;
        } else if(diaprog == 260) {
          owner.dialog.str = "I would be picked up, soon.";
          owner.dialog.displayIcon = false;
          owner.indialog = true;
          owner.diapause = false;
        } else if(diaprog == 261) {
          owner.dialog.str = "Very soon...";
          owner.dialog.displayIcon = false;
          owner.indialog = true;
          owner.diapause = false;
          FlxTimer.start(2.5, function(timer:FlxTimer):Void {
            diaprog = 20000;
            });
        } else if(diaprog == 20000) {
          owner.dialog.str = "Starvation isn't a legal penalty for second-degree murder...";
          owner.dialog.icon = 0;
          owner.dialog.displayIcon = true;
          owner.indialog = true;
          owner.diapause = false;
          FlxTimer.start(1, function(timer:FlxTimer):Void {
            owner.end();
            });
        }
        if(diaprog != 255 && diaprog != 19999) {
          diaprog++;
        }
      }
    }
    if(badtime >= 0 && diaprog < 68) {
      owner.player.mobile = false;
      owner.player.velocity.set();
      owner.player.animation.play("idle");
      if(FlxG.keyboard.pressed('ENTER')) {
        owner.indialog = false;
        owner.diapause = false;
        owner.dialog.clear();
        owner.player.mobile = true;
        remove(mrbad);
        mrbad = null;
        add(twinkle);
        diaprog = 68;
      }
      if(!owner.indialog) {
        badtime += FlxG.elapsed;
        if(badtime > 1) {
          if(badtime < 1.5) {
            if(diaprog < 65) {
              mrbad.animation.play("uhoh");
              diaprog = 65;
            }
          } else if(badtime < 2) {
            if(diaprog < 66) {
              owner.dialog.str = "Huh? Is that a... Uh-oh.";
              owner.dialog.icon = 2;
              owner.dialog.displayIcon = true;
              owner.indialog = true;
              diaprog = 66;
            }
          } else if(badtime < 3) {
            if(diaprog < 67) {
              mrbad.animation.play("stand");
              diaprog = 67;
            }
          } else if(mrbad.onScreen()) {
            mrbad.animation.play("walk");
            mrbad.velocity.x = 60;
          } else {
            remove(mrbad);
            mrbad = null;
            add(twinkle);
            owner.player.mobile = true;
            diaprog = 68;
          }
        }
      }
    }
  }
  override public function destroy():Void {
    super.destroy();
    mrbad = twinkle = door = null;
  }
  override public function fresh():Level {
    return new Level1(owner);
  }
  override public function shoot():Bool {
    if(mrbad != null  && mrbad.onScreen()) {
      owner.indialog = false;
      owner.diapause = false;
      owner.dialog.clear();
      owner.player.mobile = false;
      owner.player.acceleration.x = 0;
      owner.player.velocity.set();
      add(new DeadBad(mrbad.x, mrbad.y));
      remove(mrbad);
      mrbad = null;
      diaprog = 255;
      FlxTimer.start(2, function(timer:FlxTimer):Void {
        diaprog = 256;
        });
      FlxG.sound.music.stop();
      return true;
    }
    return false;
  }
}
