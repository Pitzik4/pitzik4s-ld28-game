package pitzik4.games.ld48.ld28.lvl;

import flixel.FlxG;
import flixel.FlxSprite;
import flixel.FlxObject;
import pitzik4.games.ld48.ld28.TriggerZone;

class Level2 extends Level {
  inline public static var lvl2:String = "assets/lvl/lvl2.csv";
  inline public static var SndFall:String = "assets/snd/fall.wav";
  
  public var hittime:Float = -1;
  public var diaprog:Int = 0;
  
  public var door:FlxSprite;
  
  public function new(owner:PlayState) {
    super(lvl2,owner);
    var edoor:FlxSprite = new FlxSprite(16*32,9*32);
    edoor.loadGraphic(Level1.ImgDoodads, true, false, 32, 32);
    edoor.animation.add("closed", [3]);
    edoor.animation.add("open", [4]);
    edoor.animation.play("closed");
    edoor.immovable = true;
    edoor.allowCollisions = FlxObject.NONE;
    add(edoor);
    var trig:TriggerZone = new TriggerZone(owner.player,34,13,1,2,function():Void {
      owner.dialog.str = "Hold down the jump key for longer to jump higher.";
      owner.dialog.displayIcon = false;
      owner.indialog = true;
      owner.diapause = true;
      });
    add(trig);
    trig = new TriggerZone(owner.player,40,10,1,1,function():Void {
      owner.dialog.str = "I could probably jump up onto that ledge from underneath.";
      owner.dialog.icon = 1;
      owner.dialog.displayIcon = true;
      owner.indialog = true;
      owner.diapause = true;
      });
    add(trig);
    trig = new TriggerZone(owner.player,45,14.75,1,1,function():Void {
      FlxG.sound.play(SndFall);
      FlxG.camera.shake(0.05,0.25);
      owner.player.mobile = false;
      owner.player.velocity.set();
      owner.player.acceleration.x = 0;
      owner.player.superanim = "liedown";
      hittime = 0;
      });
    add(trig);
    
    door = new FlxSprite(49*32,14*32);
    door.loadGraphic(Level1.ImgDoodads, true, false, 32, 32);
    door.animation.add("closed", [3]);
    door.animation.add("open", [4]);
    door.animation.play("closed");
    door.immovable = true;
    door.allowCollisions = FlxObject.NONE;
    add(door);
    trig = new TriggerZone(owner.player,49,14,1,1,function():Void {
      door.animation.play("open");
      FlxG.sound.play(Level1.SndOpen);
      FlxG.camera.fade(0xFF000000, 1.5, false, function():Void {
        FlxG.camera.fade(0xFF000000, 1.5, true, null, true);
        owner.switchLevel(new Level3(owner));
        destroy();
        });
      });
    add(trig);
  }
  override public function begin():Void {
    owner.player.setPosition(16*32,9*32);
  }
  override public function update():Void {
    super.update();
    if(FlxG.keyboard.pressed('ENTER')) {
      owner.indialog = false;
      owner.diapause = false;
      owner.dialog.clear();
      owner.player.mobile = true;
      owner.player.superanim = null;
      diaprog = 10;
      hittime = 10;
    }
    if(hittime >= 0 && !owner.indialog) {
      hittime += FlxG.elapsed;
      if(hittime >= 3) {
        if(diaprog <= 0) {
          diaprog = 1;
          owner.dialog.str = "...Ouch.";
          owner.dialog.icon = 1;
          owner.dialog.displayIcon = true;
          owner.indialog = true;
          owner.diapause = true;
        } else if(hittime >= 5 && diaprog <= 1) {
          diaprog = 2;
          owner.player.mobile = true;
          owner.player.superanim = null;
          owner.dialog.str = "That wasn't very smart.";
          owner.dialog.icon = 0;
          owner.dialog.displayIcon = true;
          owner.indialog = true;
          owner.diapause = true;
        }
      }
    }
  }
  override public function destroy():Void {
    super.destroy();
  }
  override public function fresh():Level {
    return new Level2(owner);
  }
}
